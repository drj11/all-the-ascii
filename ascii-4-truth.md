# The Truth

When you `cat` a file and display it on the Terminal
`cat` doesn't do anything sophisticated,
it just takes the byte contents of the file and
writes them sequentially to `stdout`.

This can be problematic,
because while a lot of characters will display in an ordinary manner,
many do not.

The ASCII _Control Characters_ are not ordinary characters.
By ordinary characters I mean characters like «A» or «$» that
get printed out when you display them.
The ASCII control characters were traditionally used to
_control_ either the print head
(move it, without printing anything) or
the communication flow (to start and stop a transmission).

That means that when you `cat` a file that contains
control characters, you might not "see" the characters in the ordinary sense.

What can you do?

There are a variety of Unix tools that can help you.
They can be particularly useful if you are trying to track down problems
with line-endings, embedded control characters, or encodings.

## Containing Control Characters

First, let's create a file that has some control characters in,
so that you can see the difference in the output of the tools.

Type this dinky little command:

    printf 'food\rbar\n' > foob

That creates a file call `foob` in your current directory.

Try running `foob` on this file.

    cat foob

It's probably not harmful.
It should display the word `bard`.

What's going on?
Where did the `d` come from, and where did the `foo` go?

You might have guessed that it has something to do with
the `\r` and `\n` sequences in that `printf` command.
It does.

In `printf` `\r` means "insert the ASCII Carriage Return character",
and `\n` means "insert the ASCII Line Feed character" (also called "newline").

When `cat` prints the file `foob` it first prints `f` `o` `o` `d`
and then "prints" the Carriage Return character.
This is a _control character_ that
has the effect of moving the print position
(which would, at one point in history,
have been the carriage of the print-head of a printer)
to the beginning of the line.
`cat` then continues to print `b` `a` `r` and these letters replace the
previously printed `f` `o` `o`.
The `d` is still left and hasn't replaced by another letter.
So the final result is the word `bard`.

The control characters have deceived us.
How can we tell what the true contents of the file are?

## Visualise

You have many options and which one is best is a matter of taste
and ultimately what you are trying to achieve.

    od -t x1a   # POSIX: ultimate portability

    od -c       # XSI

    sed -n l

    vis         # UNIX 8th Edition

    less        # GNU utils

    vi

    nano

    ed

    cat -v      # despicable

I'll show these in a gallery, then follow with some discussion

	$ od -t x1a foob
	0000000  66  6f  6f  64  0d  62  61  72  0a
	          f   o   o   d  cr   b   a   r  nl
	0000011


	$ od -c foob
	0000000   f   o   o   d  \r   b   a   r  \n
	0000011
	
I put `od -t x1a` not because it is the easiest or the most practical,
but because it is the most widely available and shows the most "truth".

`od` (short for octal dump) shows 16 bytes per row.
In this case the file is shorter than 16 bytes, so the row is truncated.
The first row of output starts with a file address
(a byte offset from the beginning of the file),
followed by the hexadecimal codes for each byte of the file.
The next row of output shows the same bytes but this time in
named ASCII notation.
You can see that between `foo` and `bar`
there is a character called `esc`
(for escape).
You can also see that the very final character of the file
is not the `r` in `bar` but a character called `nl`.
`nl` is short for _newline_, but POSIX also permits `lf` (linefeed)
to be used here.

The final row of output is size of the file.

Note that in this case `od` gives the byte offset in octal (base 8),
which is traditional.

`od` had many many ways of configuring it.
The `-t` option controls the row-by-row output,
it's all incredibly bizarre and baroque.

    $ od -c foob
    0000000   f   o   o 033   b   a   r  \n
    0000010

od -c

Shorter to type, shorter output
(because there is only one row for each 16-byte block),
and this time the `esc` character is displayed as `033` (octal)
and newline is displayed as `\n` (in honour of C's string syntax).

## Terminal, why tho?

Incidentally, why is called a terminal?
These days, when I say _terminal_ I mean a computer program that
reads and writes text and simulates an old-style terminal.
Even now, most of them follow the basic model of the Teletype
Model 33 ASR (Automatic Send and Receive).
This had a (typewriter style) keyboard and a printer,
and typically were used to communicate to a central office
computer via a phone line.
From the perspective of the central computer,
the phone line _terminated_ at the keyboard/printer equipment,
and so the keyboard/printer combination was the _terminal_ equipment.

## REFERENCES

[] http://harmful.cat-v.org/cat-v/
