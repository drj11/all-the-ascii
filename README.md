# A series of Three Articles

Comprehensively covering the software aspects of ASCII
and ASCII-compatible codes (such as Unicode / UTF-8),
giving inquisitive learners complete confidence
in inspecting and manipulating text documents.

Overview of ASCII codes: Let's learn ASCII.

ASCII input: How to deliberately create control codes.

ASCII truth: How to detect control codes in files.

## Let's learn ASCII

Ideas to cover:
- a character is _represented_ by a number
- many ways
- ASCII is one way to do this
- it doesn't cover all characters and symbols
- there are extensions of ASCII that do, eg unicode, not covered
- control characters are code points that are not printable symbols

## ASCII input

- Control-V
- ISO 14755, Control-Shift-U

## ASCII truth

- od -ba
- ob -t x1 -A d
