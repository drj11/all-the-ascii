# ASCII Codes

Almost every statement below is not quite true,
but true enough to be useful.
Discussion of truth left to an appendix.

Computers are really good at handling numbers.
A file is a sequence of bytes.
A byte is 8 bits, and represents a number between 0 and 255.

How do we represent letters and text and stuff?
Using ASCII. Like this:

	 !"#$%&'()*+,-./0123456789:;<=>?
	@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_
	`abcdefghijklmnopqrstuvwxyz{|}~

Each row represents 32 ASCII symbols,
starting with SPACE « ».
Each symbol is assigned a number.
And we can represent text, by converting the text into a
sequence of symbols, and
then mapping the symbols to numbers,
and storing the sequence of numbers in a file.

So SPACE gets code 1, exclamation point «!» gets code 2?
No.
Nothing is ever that simple.

SPACE gets code 32, «!» gets 33, and so on.
«A» through «Z» gets codes from 65 to 90.
«a» through «z» gets codes from 97 to 122.

The ten digits «0» to «9» are also in ASCII,
using codes 48 to 57.

Isn't this all a bit weird?

I said earlier that computers are good at handling numbers.
By which I meant binary numbers.
Binary numbers only use 0 and 1.
Conventional right-to-left place-value notation is used.

65₁₀ (decimal) is 1000001₂ (binary).
These are two different ways of writing the same number.
Borrowing a convention from mathematics,
I've written a subscript 10 (for base 10) after the number in
decimal,
and a subscript 2 (for base 2) after the number in binary.

97₁₀ is 1100001₂.

«A» is 1000001₂; «a» is 1100001₂.

Note that these differ by a single digit
(technically, being a binary number, I should call this a _bit_,
but technically technically,
I can also use _digit_ to mean value symbol in bases other than 10).
So this explains some of the funky numbering.

«A» is 2**6 + 1, «B» is 2**6 + 2, and so on.
The lowercase letters all have codes that are 2**5 (32) higher.

So we can see that the binary coding has had an influence on
which codes are used for which symbols in ASCII.

We put the letters A-Z in a block of 32,
and put the lowercase equivalents in another block of 32.
That leaves 6 gaps in each block, which we fill with
punctuation.

## Bytes

A byte is 8 bits (binary digit).
Historically, and in some current communities,
a byte could have more or fewer bits, but these days,
it's 8 bits.

ASCII codes are almost always stored in bytes,
and to emphasise this,
it is common to write out the numbers using 8 bits:

    01000001  A
    01000010  B

and so on.
As it happens ASCII only uses codes from 0 to 127, so in an 8-bit byte,
the left-hand, most significant, bit will always be 0.

The problem with binary is that all the numbers are really long.

Those 8-bit numbers only go up to 255₁₀.

## Let's learn hexadecimal!

For various reasons it's better to clump the bits together.
_Hexadecimal+, base-16, does that.
Each digit represents a number from 0 to 15,
and the places increase in powers of 16
(instead of 10 like in decimal, or 2 in binary).

It's a traditional compromise between
an accurate model of the computer state,
and being convenient for humans.

Because 16 is a power of 2, it's 2**4,
it's straightforward to convert from binary to hexadecimal.
Split the bits into groups of 4, then convert each group:

    01000001₂

becomes

    0100 0001₂

becomes

    41₁₆

For the hexadecimal digits from 10 to 15,
it is conventional to use letters «A» to «F»
(often in either upper or lower case).

In programming languages there are various syntaxes for using
hexadecimal numbers.
C has «0x»

Returning to ASCII...

The letter «A» has code 65₁₀, which is 41₁₆.

The letter «B» has code 66₁₀, which is 42₁₆.

...

The letter «I» has code 73₁₀, which is 49₁₆.

The letter «J» has code 74₁₀, which is 4A₁₆.

## ASCII in hexadecimal

A lot of ASCII makes a bit more sense in hexadecimal.

Upper case letters are 41₁₆ to 5A₁₆.

Lower case letters are 61₁₆ to 7A₁₆.

The numbers 0 to 9 are 30₁₆ to 39₁₆

Here's the rest of the ASCII table:


         0123456789ABCDEF

    20₁₆  !"#$%&'()*+,-./
    30₁₆ 0123456789:;<=>?
    40₁₆ @ABCDEFGHIJKLMNO
    50₁₆ PQRSTUVWXYZ[\]^_
    60₁₆ `abcdefghijklmno
    70₁₆ pqrstuvwxyz{|}~

The first hexadecimal digit («0» to «F»)
can be read off the left-hand margin;
the second hexadecimal digit (also «0» to «F»)
can be read off the top margin.

So, for example, we can see that «:» has ASCII code 3A₁₆
(it appears in the second row, just after all the numbers).

